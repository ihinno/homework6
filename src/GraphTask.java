import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class GraphTask {

	public static void main(String[] args) {
		GraphTask a = new GraphTask();
		a.run();

	}

	public void run() {
		Graph g = new Graph("G");
		int n = 500;
		int m = (int) (n * 1.2);
		int source = (int) (Math.random() * n);
		int dest = (int) (Math.random() * n);
		g.createRandomSimpleGraph(n, m);
		System.out.println(g);
		Vertex s = g.getVertexList().get(source);
		System.out.println("start @ " + s.toString());
		Vertex f = g.getVertexList().get(dest);
		System.out.println("finish @ " + f.toString());
		g.breadthFirst(s, f);

		// TODO!!! Your experiments here
	}

	class Vertex {

		private String id;
		private Vertex next;
		private Arc first;
		private int info = 0;
		private int dist;
		private Vertex previous;

		Vertex(String s, Vertex v, Arc e) {
			id = s;
			next = v;
			first = e;
		}

		Vertex(String s) {
			this(s, null, null);
		}

		@Override
		public String toString() {
			StringBuilder vertex = new StringBuilder();
			vertex.append(id);
			// vertex.append(" ");
			// vertex.append(info);
			// vertex.append(" ");
			// vertex.append(dist);
			// vertex.append(" ");
			// vertex.append(previous.id);
			return vertex.toString();
		}

		public void setVInfo(int i) {
			this.info = i;// TODO Auto-generated method stub

		}

		public void setVdist(int i) {
			this.dist = i;// TODO Auto-generated method stub

		}

		public void setVprev(Vertex v) {
			this.previous = v;// TODO Auto-generated method stub

		}

		public Vertex getNext() {
			return next;
		}

		public ArrayList<Arc> outArcs() {
			ArrayList<Arc> arcs = new ArrayList();
			Arc arc = first;
			arcs.add(arc);
			while (arc.next != null) {
				arc = arc.next;
				arcs.add(arc);
			}
			return arcs;
		}

		public int getVInfo() {
			// TODO Auto-generated method stub
			return info;
		}

		// TODO!!! Your Vertex methods here!
	}

	class Arc {

		private String id;
		private Vertex target;
		private Arc next;
		private int info;

		Arc(String s, Vertex v, Arc a) {
			id = s;
			target = v;
			next = a;
		}

		Arc(String s, Vertex v, Arc a, int i) {
			id = s;
			target = v;
			next = a;
			info = i;
		}

		Arc(String s) {
			this(s, null, null);
		}

		@Override
		public String toString() {
			return id;
		}

		public Vertex getToVert() {
			// TODO Auto-generated method stub
			return target;
		}

		// TODO!!! Your Arc methods here!
	}

	class Graph {

		private String id;
		private Vertex first;
		private int info = 0;

		Graph(String s, Vertex v) {
			id = s;
			first = v;
		}

		Graph(String s) {
			this(s, null);
		}

		public Vertex getFirst() {
			return first;
		}

		@Override
		public String toString() {
			String nl = System.getProperty("line.separator");
			StringBuffer sb = new StringBuffer(nl);
			sb.append(id);
			sb.append(nl);
			Vertex v = first;
			while (v != null) {
				sb.append(v.toString());
				sb.append(" -->");
				Arc a = v.first;
				while (a != null) {
					sb.append(" ");
					sb.append(a.toString());
					sb.append(" (");
					sb.append(v.toString());
					sb.append("->");
					sb.append(a.target.toString());
					sb.append(")");
					a = a.next;
				}
				sb.append(nl);
				v = v.next;
			}
			return sb.toString();
		}

		public Vertex createVertex(String vid) {
			Vertex res = new Vertex(vid);
			res.next = first;
			first = res;
			return res;
		}

		public Arc createArc(String aid, Vertex from, Vertex to) {
			Arc res = new Arc(aid);
			res.next = from.first;
			from.first = res;
			res.target = to;
			return res;
		}

		/**
		 * Create a connected undirected random tree with n vertices. Each new
		 * vertex is connected to some random existing vertex.
		 * 
		 * @param n
		 *            number of vertices added to this graph
		 */
		public void createRandomTree(int n) {
			if (n <= 0)
				return;
			Vertex[] varray = new Vertex[n];
			for (int i = 0; i < n; i++) {
				varray[i] = createVertex("v" + String.valueOf(n - i));
				if (i > 0) {
					int vnr = (int) (Math.random() * i);
					createArc("a" + varray[vnr].toString() + "_" + varray[i].toString(), varray[vnr], varray[i]);
					createArc("a" + varray[i].toString() + "_" + varray[vnr].toString(), varray[i], varray[vnr]);
				} else {
				}
			}
		}

		/**
		 * Create an adjacency matrix of this graph. Side effect: corrupts info
		 * fields in the graph
		 * 
		 * @return adjacency matrix
		 */
		public int[][] createAdjMatrix() {
			info = 0;
			Vertex v = first;
			while (v != null) {
				v.info = info++;
				v = v.next;
			}
			int[][] res = new int[info][info];
			v = first;
			while (v != null) {
				int i = v.info;
				Arc a = v.first;
				while (a != null) {
					int j = a.target.info;
					res[i][j]++;
					a = a.next;
				}
				v = v.next;
			}
			return res;
		}

		/**
		 * Create a connected simple (undirected, no loops, no multiple arcs)
		 * random graph with n vertices and m edges.
		 * 
		 * @param n
		 *            number of vertices
		 * @param m
		 *            number of edges
		 */
		public void createRandomSimpleGraph(int n, int m) {
			if (n <= 0)
				return;
			if (n > 2500)
				throw new IllegalArgumentException("Too many vertices: " + n);
			if (m < n - 1 || m > n * (n - 1) / 2)
				throw new IllegalArgumentException("Impossible number of edges: " + m);
			first = null;
			createRandomTree(n); // n-1 edges created here
			Vertex[] vert = new Vertex[n];
			Vertex v = first;
			int c = 0;
			while (v != null) {
				vert[c++] = v;
				v = v.next;
			}
			int[][] connected = createAdjMatrix();
			int edgeCount = m - n + 1; // remaining edges
			while (edgeCount > 0) {
				int i = (int) (Math.random() * n); // random source
				int j = (int) (Math.random() * n); // random target
				if (i == j)
					continue; // no loops
				if (connected[i][j] != 0 || connected[j][i] != 0)
					continue; // no multiple edges
				Vertex vi = vert[i];
				Vertex vj = vert[j];
				createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
				connected[i][j] = 1;
				createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
				connected[j][i] = 1;
				edgeCount--; // a new edge happily created
			}
		}

		// Koostada meetod, mis leiab etteantud sidusas lihtgraafis kahe
		// etteantud tipu vahelise l�hima tee

		// TODO!!! Your Graph methods here!

		/**
		 * Traverse the graph breadth-first. Execute vertWork (v) for each
		 * vertex v.
		 * 
		 * @param s
		 *            source vertex
		 */
		// http://cseweb.ucsd.edu/~kube/cls/100/Lectures/lec11.graphalgos/lec11-26.html
		public void breadthFirst(Vertex s, Vertex f) {
			if (getVertexList() == null)
				return;
			if ((!getVertexList().contains(s)) | (!getVertexList().contains(f)))
				throw new RuntimeException("the argument vertices are not represented in the graph!");
			this.initgraph();
			List<Vertex> vq = Collections.synchronizedList(new LinkedList());
			vq.add(s);
			s.setVInfo(1); // "gray"
			while (vq.size() > 0) {
				Vertex v = (Vertex) vq.remove(0); // breadth == FIFO
				v.setVInfo(2); // "black"
				//vertWork(v);
				ArrayList<Arc> eit = v.outArcs();
				for (Iterator iterator = eit.iterator(); iterator.hasNext();) {
					Arc e = (Arc) iterator.next();
					// System.out.println(" Arcs " + e.toString());
					Vertex w = e.getToVert();
					if (w.getVInfo() == 0) {
						w.dist = v.dist + 1;
						w.previous = v;
						vq.add(w);
						w.setVInfo(1);
					}
				}
			}
			// http://stackoverflow.com/questions/17480022/java-find-shortest-path-between-2-points-in-a-distance-weighted-map
			ArrayList<Vertex> directions = new ArrayList<Vertex>();
			StringBuilder path = new StringBuilder();
			for (Vertex vertex = f; vertex != s; vertex = vertex.previous) {
				directions.add(vertex);
			}
			directions.add(s);
			int hops = directions.get(directions.indexOf(f)).dist;
			Collections.reverse(directions);

			for (Vertex vertex : directions) {
				path.append(vertex.id + " -> ");
			}

			System.out.println();
			System.out.print(path.toString());
			System.out.println(" kulus " + hops + " sammu");

		}

		private ArrayList<Vertex> getVertexList() {
			ArrayList<Vertex> list = new ArrayList<Vertex>();
			Vertex v = this.first;
			list.add(v);
			while (v.next != null) {
				list.add(v.getNext());
				v = v.getNext();
			}
			return list;
		}

		/** Work to do when traversing the graph */
		public void vertWork(Vertex v) {
			System.out.print(" " + v.toString());
		}

		public void initgraph() {
			ArrayList<Vertex> vlist = getVertexList();
			for (int i = 0; i < vlist.size(); i++) {
				vlist.get(i).setVInfo(0);// "white"
				vlist.get(i).setVdist(0);
				// vlist.get(i).setVprev(null);
			}
		}

	}
}